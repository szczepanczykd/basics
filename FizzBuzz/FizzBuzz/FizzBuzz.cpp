// FizzBuzz.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

template<int N, int X, int Z>
struct fizzbuzz : fizzbuzz<N - 1, (N - 1) % 3, (N - 1) % 5>
{
	fizzbuzz()
	{
		cout << N << endl;
	}
};


template <int N, int X>
struct fizzbuzz<N, X, 0> : fizzbuzz<N - 1, (N - 1) % 3, (N - 1) % 5>
{
	fizzbuzz()
	{
		cout << "Buzz" << endl;
	}
};

template <int N, int Z>
struct fizzbuzz<N, 0, Z> : fizzbuzz<N - 1, (N - 1) % 3, (N - 1) % 5>
{
	fizzbuzz()
	{
		cout << "Fizz" << endl;
	}
};

template <int N>
struct fizzbuzz<N, 0, 0> : fizzbuzz<N - 1, (N - 1) % 3, (N - 1) % 5>
{
	fizzbuzz()
	{
		cout << "FizzBuzz" << endl;
	}
};

template <>
struct fizzbuzz<0,0,0>
{
	fizzbuzz()
	{
		cout << "0" << endl;
	}
};

template <int N>
struct fb_run
{
	fizzbuzz <N, N % 3, N % 5> fb;
};


int main()
{
	fb_run<100> fb;

	system("pause");
    return 0;
}

