// NameGenarator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <ctime>


using namespace std;

string NameGenerator(string name, string surname)
{
	int Nlength = name.length();
	int Slength = surname.length();
	srand(time(NULL));

	string out = "";

	if (Nlength < 4)
		out += name;
	else
	{
		Nlength = (rand() % (Nlength / 2)) + 3;
		out = name.substr(0, Nlength);
	}

	out += to_string(rand() % 10) + '.';

	if (Slength < 4)
		out += surname;
	else
	{
		Slength = (rand() % (Slength / 2)) + 3;
		out += surname.substr(0,Slength);
	}

	out += to_string(rand() % 10);
	out += to_string(rand() % 10);
	out += to_string(rand() % 10);

		return out;
}


int main()
{
	cout << NameGenerator("Name", "Surname") << endl;
	
	system("pause");
    return 0;
}

