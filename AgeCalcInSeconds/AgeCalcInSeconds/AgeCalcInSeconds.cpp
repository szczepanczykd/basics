// AgeCalcInSeconds.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <ctime>

using namespace std;


int getYear()
{
	time_t now = time(0);
	struct tm timeinfo;
	localtime_s(&timeinfo, &now);
	return timeinfo.tm_year + 1900;
}

int getMonth()
{
	time_t now = time(0);
	struct tm timeinfo;
	localtime_s(&timeinfo, &now);
	return timeinfo.tm_mon + 1;
}

int getDay()
{
	time_t now = time(0);
	struct tm timeinfo;
	localtime_s(&timeinfo, &now);
	return timeinfo.tm_mday;
}

int getHour()
{
	time_t now = time(0);
	struct tm timeinfo;
	localtime_s(&timeinfo, &now);
	return timeinfo.tm_hour;
}

int getMin()
{
	time_t now = time(0);
	struct tm timeinfo;
	localtime_s(&timeinfo, &now);
	return timeinfo.tm_min;
}

int getSec()
{
	time_t now = time(0);
	struct tm timeinfo;
	localtime_s(&timeinfo, &now);
	return timeinfo.tm_sec;
}

int checkMonth(int month)
{

	if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
	{
		return 31;
	}
	else if (month == 2)
	{
		return 28;
	}
	else
	{
		return 30;
	}
}


int countDays(int month,bool forward)
{
	int days = 0;

	if (month == 0 || month == 13)
	return 0;

		days += checkMonth(month);
		if (forward)
		return days += countDays(month + 1, forward);
		return days += countDays(month - 1, forward);
	
}

int countDaysFromYear(int year)
{
	int diff = getYear() - year ;

	int numberOfDays = countDays(getMonth() - 1, false);
	
	if (getYear() % 4 == 0)
	{
		numberOfDays++;
	}
	numberOfDays += diff / 4;


	for (int i = 1; i < diff; i++)
	{
		numberOfDays += countDays(1, true);	
	}
	numberOfDays += getDay();

	return numberOfDays;
}

int calculateDays(int year, int month, int day)
{
	int days = 0;

	days = countDaysFromYear(year);
	days += countDays(month + 1, true);
	days += (checkMonth(month) - day);

	cout << "Year: " << year << " month " << month << " day " << day << endl;

	return days;
}

double calcToSeconds(int days)
{
	cout << "Days: " << days << endl;
	


	double outVal = days * 24 * 60 * 60;
	outVal += getHour() * 60 * 60;
	outVal += getMin() * 60;
	outVal += getSec();

	cout << "Hours: " << outVal / 60.0 / 60.0 << endl;
	cout.precision(10);
	cout << "Minutes: " << outVal / 60.0 << endl;
	cout.precision(20);
	cout << "Seconds: " << outVal << endl;

	return outVal;
}

void Hello()
{
	int year;
	int month;
	int day;

	cout << "Input year of birth " << endl;
	cin >> year;
	cout << "Input month " << endl;
	cin >> month;
	cout << "Input day " << endl;
	cin >> day;


	if (getYear() <= year || year < 1950)
		cout << "MHM." << endl;
	else
	 calcToSeconds(calculateDays(year, month, day));

}

int main()
{
	Hello();

	system("pause");
    return 0;
}

