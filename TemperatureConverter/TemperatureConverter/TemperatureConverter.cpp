// TemperatureConverter.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>

using namespace std;

float CelciusToKelvin(float temperature)
{
	return temperature + 273.15;
}
float KelvinToCelcius(float temperature)
{
	return temperature - CelciusToKelvin(0);
}
float CelciusToFahrenheit(float temperature)
{
	return (9.0 / 5.0) * temperature + 32;
}
float FahrenheitToCelcius(float temperature)
{
	return roundf( 5.0 / 9.0 * (temperature - 32.0) * 100.0 ) / 100.0;
}

float KevinToFahrenheit(float temperature)
{
	return  CelciusToFahrenheit(KelvinToCelcius(temperature));
}

float FahrenheitToKevin(float temperature)
{
	return CelciusToKelvin(FahrenheitToCelcius(temperature));
}

struct Celcius
{
	Celcius(float temperature)
	{
		cout << temperature << " Celcius to Kevin: " << CelciusToKelvin(temperature) << endl;
		cout << temperature << " Celcius to Fahrenheit: " << CelciusToFahrenheit(temperature) << endl;
	}
};

struct Fahrenheit
{
	Fahrenheit(float temperature)
	{
		cout << temperature << " Fahrenheit to Celcius: " << FahrenheitToCelcius(temperature) << endl;
		cout << temperature << " Fahrenheit to Kevin: " << FahrenheitToKevin(temperature) << endl;
	}
};

struct Kevin
{
	Kevin(float temperature)
	{
		cout << temperature << " Kevin to Celcius: " << KelvinToCelcius(temperature) << endl;
		cout << temperature << " Kevin to Fahrenheit: " << KevinToFahrenheit(temperature) << endl;
	}
};

int main()
{	
	Kevin kevin(0);
	Fahrenheit fahrenheit(0);
	Celcius celcius(0);

	system("pause");
    return 0;
}

