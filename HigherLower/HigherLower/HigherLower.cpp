// HigherLower.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <iostream>
#include <ctime>


using namespace std;

enum state { EQUAL, LOWER, UPPER };

void init()
{
	srand(time(NULL));
}

int generate(int lower, int upper)
{
	if( upper > lower )
	return rand() % (upper - lower) + lower;
	return -1;
}

state check_number(int number, int generated)
{
	if (number > generated)
	{
		cout << "upper !" << endl;
		return UPPER;
	}
	else if (number < generated)
	{
		cout << "Lower !" << endl;
		return LOWER;
	}
	else
		return EQUAL;
}

void HigherLower()
{
	int lower = 0;
	int upper = 0;
	int generated = 0;

	cout << "enter the number of the lower range" << endl;
	cin >> lower;
	cout << "enter the number of the upper range" << endl;
	cin >> upper;

	generated = generate(lower, upper);

	if (generated == -1)
	{
		cout << "Bad params!" << endl;
	}
	else
	{
		int hits = 0;
		int num = 0;
		do
		{
			cout << "Input number" << endl;
			cin >> num;
			hits++;
		} while (check_number(num, generated) != EQUAL);
		cout << "Equal! You win with: " << hits << " hits!" << endl;
	}
}


void makeThrow(int number)
{
	if (number == 0)
		return;
	else
	{
		static int count = 0;
		makeThrow(number - 1);
		int value = rand() % 2;

		if (value == 0)
		{
			count++;
		}
		cout << "Round: "<< number <<" Heads: " << count << " and Tails: " << number - count << endl;
	}
}



void  HeadsTails()
{
	int number = 0;

	cout << "How much time do you want to throw coin ?";
	cin >> number;

	if (number < 1)
	{
		cout << "Bad params!" << endl;
	}
	else
	{
		makeThrow(number);
	}
}


int main()
{
	init();
	//HigherLower();

	HeadsTails();

	system("pause");
    return 0;
}

