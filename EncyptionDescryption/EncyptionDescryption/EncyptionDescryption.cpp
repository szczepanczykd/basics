// EncyptionDescryption.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <ctime>
#include <vector>

using namespace std;

void encryptChoose();
void decryptChoose();

string encrypt(string data, int seed);
string decrypt(string data, int seed);


void init()
{
	cout << "Select menu:" << endl;

	char sel;
	while (1)
	{
	cout << " 1. Encrypt String\n 2. Descrypt String\n";

	cin >> sel;

		switch (sel)
		{
		case '1':
			encryptChoose();
			break;
		case '2':
			decryptChoose();
			break;
		default:
			cout << "Error !" << endl;
			return;
		}
		cout << '\n';
	}
}


void encryptChoose()
{
	string input = "";

	time_t now = time(0);
	srand(time(0));
	struct tm timeinfo;
	localtime_s(&timeinfo, &now);

	int first = timeinfo.tm_sec;
	
	cout << "Give String:" << endl;
	cin.ignore();
	getline(cin, input);
	

	cout << "Seed is: " << first << endl;
	cout << "Save if you want to decrypt message !" << endl;

	cout << encrypt(input, first) <<endl;
	
}

string encrypt(string data, int seed)
{

	string out = "";

	for (int i = 0; i < data.length(); ++i)
	{
		if (data.at(i) == ' ')
		{
			out += '"';
		}
		else
		out += (static_cast <char> (data.at(i) - seed % (2 + i)));
	}

	return out;
}



string decrypt(string data, int seed)
{

	string out = "";

	for (int i = 0; i < data.length(); ++i)
	{
		if (data.at(i) == '"')
		{
			out += ' ';
		}
		else
		out += (static_cast <char> (data.at(i) + seed % (2 + i)));
	}


	return out;
}


void decryptChoose()
{
	string input = "";
	int first = 0;

	cout << "Give String" << endl;
	cin.ignore();
	getline(cin, input);
	cout << "Input seed" << endl;
	cin >> first;

	
	cout << decrypt(input, first) <<endl;
}


int main()
{

	init();

	system("pause");
    return 0;
}

